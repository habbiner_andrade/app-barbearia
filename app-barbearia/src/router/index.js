import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home.vue'
import Cadastro from '../views/Cadastro.vue'
import CadastroLogin from '../views/CadastroLogin.vue'
import Login from '../views/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/cadastro',
    name: 'Cadastro',
    component: Cadastro
  },
  {
    path: '/cadastro_login',
    name: 'CadastroLogin',
    component: CadastroLogin
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
